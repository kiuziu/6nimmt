package kiuziu.sixTakes;

import java.util.Vector;
import static kiuziu.sixTakes.GlobalVarsAndConstants.MAX_CARDS_IN_A_ROW;

public class Row {

	private Vector<Card> cardRow = new Vector<>();
	
	public Row(Card card) {
		append(card);		
	}

	public boolean append(Card card) {
		if(cardRow.size() < MAX_CARDS_IN_A_ROW) return cardRow.add(card);
		return false;
	}
	
	public int numberOfCards() {
		return cardRow.size();
	}
	
	public int value() {
		int result = 0;
		for(Card c:cardRow) {
			result += c.getValue();
		}
		return result;
	}
	
	public int maxNumber() {
		if(cardRow.isEmpty()) return 0;
		return cardRow.lastElement().getNumber();
	}
	
	public Card remove() {
		if(cardRow.isEmpty()) return null;
		return cardRow.remove(cardRow.size()-1);
	}
	
	public boolean isEmpty() {
		return cardRow.isEmpty();
	}
	
	public Card getCard(int pos) {
		if(pos < 0 || pos > (cardRow.size() - 1)) return null;
		return cardRow.get(pos);
	}
}
