package kiuziu.sixTakes;

public class ComputerPlayer extends Player{
	
	private Strategy strategy = Strategy.EASY;
	
	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	public ComputerPlayer() {
		super(ComputerNames.randomName());
	}

	@Override
	public Card selectCard(Row[] rows) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int chooesRow(Row[] rows) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void infoCard(Player p, Card card) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void infoRow(Player p, Row row) {
		// TODO Auto-generated method stub
		
	}

}
