package kiuziu.sixTakes;

import java.util.Vector;

public abstract class Player {
	
	private String name;
	private int points = 0;
		
	private Vector<Card> handCards = new Vector<>();
	private Vector<Card> badCards = new Vector<>();
	
	public Player(String name) {
		this.name = name;
	}
	public abstract Card selectCard(Row[] rows);
	public abstract int chooesRow(Row[] rows);
	public abstract void infoCard(Player p, Card card);
	public abstract void infoRow(Player p, Row row);
	
	public boolean add(Card card) {		
		return handCards.add(card);
	}
	
	public boolean take(Card card) {
		points += card.getValue();
		return badCards.add(card);
	}
	
	public Card back() {
		if(badCards.isEmpty()) return null;
		return badCards.remove(badCards.size() - 1);
	}

	public int getPoints() {
		return points;
	}

	public String getName() {
		return name;
	}
	
	public Vector<Card> getCards(){
		return handCards;
	}
}
