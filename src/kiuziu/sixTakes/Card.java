package kiuziu.sixTakes;

public class Card implements Comparable<Card>{

	private int number;
	
	public Card(int number) {
		if(number < 1 || number > 104) 
			throw new IllegalArgumentException("Number must be between 1 and 104 (both incl.)");
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	public int getValue() {
		int value = 1;
		if(number % 5 == 0) value = 2;
		if(number % 10 == 0) value = 3;
		if(number % 11 == 0) value = 5;
		if(number == 55) value = 7;
		return value;
	}

	@Override
	public int compareTo(Card c) {
		if(this.number < c.number) return -1;
		if(this.number > c.number) return 1;
		return 0;
	}
}
