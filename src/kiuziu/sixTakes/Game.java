package kiuziu.sixTakes;

import static kiuziu.sixTakes.GUI.PlayerDialog.initPlayers;
import static kiuziu.sixTakes.GlobalVarsAndConstants.MAX_CARDS;
import static kiuziu.sixTakes.GlobalVarsAndConstants.MAX_CARDS_PER_PLAYER;
import static kiuziu.sixTakes.GlobalVarsAndConstants.MAX_ROWS;

import java.util.Collections;
import java.util.Vector;

import kiuziu.sixTakes.GUI.GameListener;
import kiuziu.sixTakes.GUI.MainFrame;

public class Game {

	private Vector<Card> cards = new Vector<>();
	private Vector<Player> players = new Vector<>();
	private Row[] rows = new Row[MAX_ROWS];
	
	private GameListener gameListener;

	public Game() {
		init();
		MainFrame.show(players, rows);
	}

	public void init() {
		cards = createCardDeck(cards);
		mixTheDeck(cards);
		ComputerNames.initNames();
		players = initPlayers(players);
		giveCards(cards, players);
		rows = initRows(cards);	
	}

	private void giveCards(Vector<Card> cards, Vector<Player> players) {
		Card c;
		for(Player p : players) {
			for (int i = 0; i < MAX_CARDS_PER_PLAYER; i++) {
				c = cards.remove(0);
				p.add(c);
			}
			Collections.sort(p.getCards());
		}
	}

	private Row[] initRows(Vector<Card> cards) {
		Row[] rows = new Row[4];
		for (int i = 0; i < MAX_ROWS; i++) {
			rows[i] = new Row(cards.remove(0));
		}
		return rows;
	}

	private Vector<Card> createCardDeck(Vector<Card> cards) {
		for (int i = 0; i < MAX_CARDS; i++) {
			cards.add(new Card(i + 1));
		}
		return cards;
	}

	private void mixTheDeck(Vector<Card> cards) {
		Collections.shuffle(cards);
	}

	public void start() {
		// TODO Auto-generated constructor stub
	}

	public void next() {
		// TODO Auto-generated constructor stub
	}

	public GameListener getGameListener() {
		return gameListener;
	}

	public void setGameListener(GameListener gameListener) {
		this.gameListener = gameListener;
	}
}
