package kiuziu.sixTakes;

public class GlobalVarsAndConstants {

	public static final int MAX_ROWS = 4;
	public static final int MAX_CARDS_IN_A_ROW = 5;
	public static final int MAX_CARDS = 104;
	public static final int MAX_PLAYERS = 10;
	public static final int MAX_CARDS_PER_PLAYER = 10;
}
