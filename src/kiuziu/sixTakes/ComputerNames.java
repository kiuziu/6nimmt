package kiuziu.sixTakes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComputerNames {

	private static List<String> rowRoboNames = new ArrayList<>();
	
	private ComputerNames() {}
	
	public static void initNames() {
		rowRoboNames.add("R2-D2");
		rowRoboNames.add("HAL 9000");
		rowRoboNames.add("Chippy");
		rowRoboNames.add("T-1000");
		rowRoboNames.add("Wall-E");
		rowRoboNames.add("Ash");
		rowRoboNames.add("Gort");
		rowRoboNames.add("Bumblebee");
		rowRoboNames.add("RoboCop");
		rowRoboNames.add("Nummer Fünf");
		rowRoboNames.add("Bender");
		rowRoboNames.add("Marvin");
		rowRoboNames.add("EVE");
	}

	public static String randomName() {
		if(rowRoboNames.isEmpty()) initNames();
		Collections.shuffle(rowRoboNames);
		return "PC: " + rowRoboNames.remove(0);
	}
}
