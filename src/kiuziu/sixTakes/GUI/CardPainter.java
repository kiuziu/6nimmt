package kiuziu.sixTakes.GUI;

import static kiuziu.sixTakes.GlobalVarsAndConstants.MAX_CARDS;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import kiuziu.sixTakes.Card;

public class CardPainter {
	
	private JLabel cardpic = new JLabel();
	private Icon[] icons = new Icon[MAX_CARDS];
	
	public static final String BILD_VERZEICHNIS = System.getProperty( "user.dir" ) + "/icons/";
	
	public CardPainter() {
		for(int i = 0; i < icons.length; i++) {
			icons[i] = new ImageIcon(BILD_VERZEICHNIS + "wurfel" + (i + 1) + ".jpg");
		}
	}

	public JLabel paintCard(Card card) {
		cardpic.setBounds(0, 0, 40, 80);
		cardpic.setIcon(icons[card.getNumber()]);
		cardpic.setText("" + card.getNumber());
		return cardpic;
	}
	
}
