package kiuziu.sixTakes.GUI;
 
import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import kiuziu.sixTakes.Card;
import kiuziu.sixTakes.Player;
 
public class PlayersTable {

	
    private PlayersTable() { }

	public static JPanel getPlayersTable(Vector<Player> players) {
		JPanel pane = new JPanel();
		int numberOfPlayers = players.size();
		int numberOfCards = countMaxCards(players);
		String[] columnNames = new String[numberOfPlayers];     
        int rowForSum = 1;
        int rowForPoints = 1;
		Object[][] data = new Object[rowForPoints + numberOfCards + rowForSum][numberOfPlayers];
        int column = 0;      
        for(Player p : players) {
        	int cardNumber = 1;
        	int sum = 0;
        	columnNames[column] = p.getName();
        	data[0][column] = "Points: " + p.getPoints();
        	for(Card c : p.getCards()) {
        		data[cardNumber++][column] = "No. " + c.getNumber();
        		sum += c.getValue();
        	}
        	data[cardNumber][column] = "Sum: " + sum;
        	column++;
        }    
        final JTable table = new JTable(data, columnNames);
        table.setPreferredScrollableViewportSize(new Dimension(700, 200));
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        pane.add(scrollPane);
		return pane;
	}
 
    private static int countMaxCards(Vector<Player> players) {
		int numberOfCards = 0;
		for(Player p : players) {
			if(numberOfCards < p.getCards().size()) numberOfCards = p.getCards().size(); 
		}
		return numberOfCards;
	}
}