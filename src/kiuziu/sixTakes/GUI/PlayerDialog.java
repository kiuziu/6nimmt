package kiuziu.sixTakes.GUI;

import java.util.Vector;

import javax.swing.JOptionPane;

import kiuziu.sixTakes.ComputerPlayer;
import kiuziu.sixTakes.HumanPlayer;
import kiuziu.sixTakes.Player;
import static kiuziu.sixTakes.GlobalVarsAndConstants.*;

public class PlayerDialog {

	public static Vector<Player> initPlayers(Vector<Player> players) {
		String name = "";
		int playerNumber = 1;
		do {
			name = JOptionPane.showInputDialog("Player " + playerNumber + ": name (end = quit, Computer = PC)");
			System.out.println("[" + name + "]");
			if (name != null && !name.isEmpty() && !name.equals("end")) {
				if(name.equalsIgnoreCase("PC")) players.add(new ComputerPlayer());
				else players.add(new HumanPlayer(firstUpperCase(name)));
				playerNumber++;
			}
		} while (name != null && !name.equals("end") && playerNumber <= MAX_PLAYERS);
		return players;
	}
	
	private static String firstUpperCase(String name) {
		String result = name.substring(0, 1).toUpperCase();
		result += name.substring(1);
		return result;
	}
}
