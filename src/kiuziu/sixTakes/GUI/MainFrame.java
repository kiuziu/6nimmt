package kiuziu.sixTakes.GUI;

import java.awt.GraphicsConfiguration;
import java.util.Vector;

import javax.swing.JFrame;

import kiuziu.sixTakes.Player;
import kiuziu.sixTakes.Row;

public class MainFrame{

	private static  GraphicsConfiguration gs;

	private static JFrame theMainFrame;
	
	public static void show(Vector<Player> players, Row[] rows) {
				
		theMainFrame = new JFrame(gs);
		
		theMainFrame.setSize(800, 600);
		theMainFrame.setLocation(100, 100);
		theMainFrame.setTitle("6 NiMMt (c) 2017");
		theMainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		theMainFrame.add(PlayersTable.getPlayersTable(players));
		
		theMainFrame.setVisible(true);
	}
		
}
