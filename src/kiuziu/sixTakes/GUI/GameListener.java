package kiuziu.sixTakes.GUI;

import kiuziu.sixTakes.Card;
import kiuziu.sixTakes.Player;
import kiuziu.sixTakes.Row;

public interface GameListener {
	
	public void startRound();
	
	public Player currentPlayer(Player player);
	
	public Card selectCard(Player player);
	
	public Card play(Player player, Card card);
	
	public Row playerSelectRow(Player player, Row row);
	
	public Row fullRow(Row row, Player player);
	
	public void dock(Row row);
	
	public void roundEnds();
	
	public void gameEnds();
}
