1  Entwurf
Um bei der Programmerstellung nicht in Sackgassen zu geraten, sollte man sich vorherüberlegen, welche Objekte (Klassen) und welche Beziehungen zwischen diesen Klassen benötigt werden. Um die erstellten Klassen auch in anderen Kontexten verwenden zu können, ist eine strikte Trennung von der graphischen Benutzerschnittstelle unabdingbar. Da Sie zum jetzigen Zeitpunkt Ihres Studiums (im Normalfall) noch keine Entwurfsmethoden kennengelernt haben, verlangen wir natürlich nicht, dass Sie einen Entwurf erstellen, der alle Methoden des Software-Engineering ausreizt. Für ein kleines Projekt, das im Rahmen dieses Praktikums erstellt werden soll, wäre dies auch maßlos übertrieben. Trotz der Größe (bzw. der Kürze) des zu erstellenden Programms ist es vorteilhaft, sich über den grundlegenden Entwurf im Klaren zu sein, bevor man mit der eigentlichen Programmierung beginnt. Um Ihnen zu zeigen, welchen Detaillierungsgrad Ihr Entwurf haben soll und um Ihnen einige Hinweise zu geben, wie man von einer gegebenen Aufgabenstellung einen Entwurf ableiten kann, werden wir dies hier an einem Beispiel vorführen. Dazu verwenden wir eine Aufgabe, die in einem der früheren Durchläufe des Praktikums verwendet wurde.

2  6 nimmt!

2.1  Aufgabenbeschreibung
Das Ziel bei dieser Aufgabe ist es, das Spiel „6 nimmt!“ zu implementieren. Es handelt sich hierbei um ein Kartenspiel für 2-10 Spieler, bei dem es darum geht, am Ende des Spiels möglichst wenige Minuspunkte zu haben. Die Applikation erlaubt es, sowohl gegen eine beliebige Anzahl von Computergegnern als auch gegen eine beliebige Anzahl von menschlichen Gegnern (am selben Computer) oder eine Kombination (bis insgesamt maximal 10) daraus zu spielen. Die momentane Spielkonfiguration soll graphisch ansprechend dargestellt werden. In verschiedenen Schwierigkeitsgraden für den Computergegner verwendet dieser unterschiedliche Strategien.

2.2  Die Regeln
Im Spiel gibt es insgesamt 104 Karten, die von 1 bis 104 durchnummeriert sind. Jede Karte hat den Wert 1 (weiße Karten) mit folgenden Ausnahmen: Karten mit Zahlen, die durch 5 teilbar sind, haben den Wert 2 (blaue Karten), Karten mit Zahlen, die durch 10 teilbar sind, haben den Wert 3 (orange Karten), Karten mit Zahlen, die durch 11 teilbar sind, haben den Wert 5 (rote Karten) und die Karte mit der Zahl 55 hat den Wert 7 (lila Karte).
Gespielt werden mehrere Spiele, solange bis ein Spieler am Ende eines Spiels mehr als 66 Punkte hat. Es gewinnt dann der Spieler, der die wenigsten Punkte hat.
Zu Beginn jedes Spiels werden die Karten gemischt. Jeder Spieler bekommt 10 Karten verdeckt ausgeteilt, die sich nur der jeweilige Spieler ansehen darf. Wie dies mit mehreren menschlichen Spielern an einem Rechner zu lösen ist, wird später erklärt.

2.2.1 Reihen bilden
Vom restlichen Kartenstapel werden die obersten 4 Karten offen in der Tischmitte ausgelegt (siehe Abbildung 2.1). Jede Karte bildet den Anfang einer Reihe, die einschließlich dieser ersten Karte auf maximal 5 Karten anwachsen darf. Der Kartenstapel, der jetzt noch übrig ist, wird erst wieder im nächsten Spiel benötigt.156034101AnlegerichtungAbbildung 2.1: Startkonfiguration

2.2.2 Spielablauf

1.Karte ausspielen
Alle Spieler legen verdeckt 1 Karte von ihren Handkarten vor sich auf den Tisch. Erst dann, wenn der letzte sich entschieden hat, werden die Karten aufgedeckt.
Wer die niedrigste Karte ausgespielt hat, legt sie als erste Karte an eine der vier Reihen, dann kommt die zweitniedrigste Karte in eine Reihe usw., bis auch die höchste Karte dieser Runde in einer Reihe untergebracht wurde. Die Karten werden in einer Reihe immer nebeneinander gelegt. Danach wiederholt sich dieser Vorgang so oft, bis alle 10 Karten ausgespielt wurden.
Wie werden die Karten zugeordnet?
Jede ausgespielte Karte passt immer nur in eine Reihe. Es gelten folgende Regeln:
–„Aufsteigende Zahlenfolge“
Die Karten einer Reihe müssen immer eine aufsteigende Zahlenfolge haben.
–„Niedrigste Differenz“
Eine Karte muss immer in die Reihe gelegt werden, deren letzte Karte die niedrigste Differenz zu der neuen Karte aufweist.

2.Karten kassieren
Solange man eine Karte in einer Reihe unterbringt, ist alles bestens. Wenn aber in eine Reihe, in die man anlegen muss, keine weitere Karte mehr passt oder wenn man eine Karte in keine Reihe anlegen kann, muss der Spieler, der die entsprechende Karte gespielt hat, Karten kassieren.

3.„Volle Reihe“
Eine Reihe ist mit 5 Karten voll. Wenn nach Regel 2 eine sechste Karte in diese Reihe gelegt werden muss, dann muss der Spieler, der diese Karte ausgespielt hat, alle 5 Karten dieser Reihe an sich nehmen. Seine sechste Karte stellt den Anfang der neuen Reihe dar.

4.„Niedrigste Karte“
Wer eine Karte ausspielt, deren Zahl so niedrig ist, dass sie in keine Reihe passt, muss alle Karten einer beliebigen Reihe nehmen und seine „niedrige“ Karte stellt dann die erste Karte dieser Reihe dar.
Karten, die kassiert werden mussten, werden mit ihren Werten als Minuspunkte für den entsprechenden Spieler angeschrieben. Diese Karten sind aus dem Spiel und werden nicht auf die Hand genommen.

2.2.3 Spielende
Das Spiel endet, wenn alle Karten ausgespielt wurden. Die Ergebnisse (Minuspunkte) werden für jeden Spieler vermerkt und es beginnt ein neues Spiel. Das Spiel endet, wenn ein Spieler insgesamt mehr als 66 Minuspunkte gesammelt hat.

2.3  Zur Umsetzung
Das Spiel ist wie in den Regeln beschrieben umzusetzen. Für die graphische Darstellung der Karten können sowohl Scans von den Originalkarten (diese werden vom Lehrgebiet nicht zur Verfügung gestellt) als auch alternative Darstellungen gewählt werden. Neben der Anzeige der Spielfläche mit den vier Kartenreihen soll für den menschlichen Spieler, der gerade an der Reihe ist, eine Anzeige der Handkarten existieren. Die Auswahl der zu spielenden Karte ist sowohl über die Maus, als auch über die Tastatur (z.B. Zahlen 0-9) zu steuern, damit bei mehreren menschlichen Spielern ebenfalls eine gewisse Geheimhaltung möglich ist. D.h., die Handkarten eines Spielers können zwar von den anderen menschlichen Spielern eingesehen werden, allerdings können die anderen Spieler nicht erkennen, welche Karte vom aktuellen Spieler durch Tastendruck ausgewählt wird.
Wenn alle Karten ausgespielt sind, beginnt die Anlegephase. Per „Weiter“-Knopf soll diese Phase für den Betrachter in einzelne Schritte aufgeteilt werden können, d.h., mit einem Druck auf diesen Knopf wird die nächste Karte angelegt. Wenn der Fall „Niedrigste Karte“ eintritt, muss der entsprechende Spieler aufgefordert werden, eine Reihe auszuwählen. Durch entsprechende Textmeldungen sollen zudem die Züge aller Spieler nochmal erläutert werden, z.B. „Spieler Jörg legt an Reihe 2 an“, „Computer-Spieler Apfel nimmt Reihe 1“. Der „Weiter-Knopf“ soll deaktiviert werden können, um ein schnelleres Spiel zu gewährleisten.
Um taktisches Spiel zu erlauben, muss zu jedem Zeitpunkt des Spiels eine Tabelle mit den Punkteständen vor Beginn des aktuellen Spiels einzusehen sein. Am Ende jedes Spiels gibt es eine zusätzliche Anzeige des aktuellen Standes.
Zu Beginn eines Spiels kann bestimmt werden, wieviele Spieler (2-10) insgesamt an dem Spiel teilnehmen und wieviele davon vom Computer gesteuert werden (auch ein Spiel
Computer-Computer soll möglich sein). Jeder Spieler kann seinen Namen eingeben. Außerdem kann entschieden werden, ob die Standard-Spielgrenze von 66 Punkten geändert werden soll (auf einen beliebigen Wert) oder ob evtl. stattdessen eine beliebige Anzahl an Runden gespielt wird. Für jeden Computer-Gegner ist eine Spielstärke auszuwählen. Zusätzlich soll es möglich sein, auch die Handkarten des Computers einzusehen, wenn er an der Reihe ist. Dies ermöglicht es, die Strategie des Computers zu überprüfen.
Für die Computer-Strategie gibt es verschiedene Überlegungen, die noch erweitert werden können und sollen:
Einfache Spielstärke: In der Kartenauswahlphase wird eine zufällige Karte gewählt. Im Fall „Niedrigste Karte“ wird immer die Reihe mit der niedrigsten Gesamtpunktzahl ausgewählt.
Mittlere Spielstärke: Bei der Kartenauswahl wählt der Computer eine Karte, von der er glaubt, dass er in der Folge keine Kartenreihe nehmen muss. Dies kann z.B. eine Karte mit niedriger Differenz zu einer Kartenreihe mit weniger als fünf Karten sein oder z.B. eine Karte mit besonders hoher Differenz zur höchsten Reihe, in der Hoffnung, dass ein anderer Spieler diese Reihe vorher nehmen muss. Es sollte mehrere solcher Regeln geben, die dann gewichtet werden und unter denen die beste ausgewählt wird. Im Fall „Niedrigste Karte“ soll der Computer bewusst gegen denjenigen von den verbleibenden Spielern spielen, der momentan die niedrigste Punktzahl hat.
Hohe Spielstärke: Der Computer merkt sich alle gespielten Karten und bezieht dieses Wissen in seine Berechnungen ein. Außerdem zählt er die aktuellen Punktzahlen auch während des Spiels mit. Bei der Kartenauswahl spielt der Computer auch bewusst Karten, um den Fall „Niedrigste Karte“ zu erzwingen, wenn er sich damit wenig schadet, aber dem führenden Spieler mehr schaden kann.
In der Dokumentation zu Ihrer Implementierung sind die von Ihnen gewählten Computerstrategien ausführlich zu erläutern.

2.4  Optionale Erweiterung
Es gibt noch eine Profivariante des Spiels für 2-6 Spieler, bei der zwei zusätzliche Regeln hinzukommen:

1.Alle Karten im Spiel sind bekannt
Man spielt nur mit so vielen Karten, wie Spieler teilnehmen. Die Regel hierfür ist: Anzahl der Spieler mal 10 plus 4 Karten. Beispiel: 3 Spieler, 34 Karten von 1-34. Alle darüberliegenden Kartennummern werden aussortiert.

2.Jeder Spieler wählt seine 10 Karten selbst aus.
Die Karten werden offen auf dem Tisch ausgelegt. Reihum, beginnend mit einem zufällig ausgewählten Spieler, nehmen sich die Spieler immer eine Karte, bis sie 10 Karten auf der Hand haben. Jetzt müssen noch 4 Karten auf dem Tisch liegen. Diese 4 Karten stellen die 4 Reihen dar.
Der weitere Ablauf des Spiels entspricht dem Grundspiel.
Je nach eingestelltem Schwierigkeitsgrad für die Computergegner soll sich hier das Verhalten der Gegner natürlich auch unterscheiden.
3Überlegungen zum Entwurf

3.1  Erkennen von Klassen und Beziehungen
Im Entwurf geht es darum, mögliche Klassen und deren Beziehungen in der Aufgabenstellung zu erkennen. Als Faustregel gilt, dass (wichtige) Substantive in der Aufgabe Kandidaten für Klassen sind. Weniger wichtige Substantive stellen häufig Attribute von Klassen dar. Verben symbolisieren oft Beziehungen zwischen den verschiedenen Klassen. Wir versuchen nun aus der Aufgabenstellung „6 nimmt!“ Klassen und deren Beziehungen herzuleiten.
Schon im ersten Satz der Aufgabestellung entdecken wir den ersten Kandidaten für eine Klasse Spiel (Game). Dies könnte eine Klasse werden, die die Koordination der anderen Klassen übernimmt. Auch der zweite Satz der Aufgabenstellung liefert uns wichtige Hinweise. Offenbar gibt es im Spiel Spieler (Player), die jeweils Minuspunkte (points) besitzen können. Die Minuspunkte lassen sich durch einfache Integerzahlen darstellen, so dass dafür keine eigene Klasse notwendig ist. Wir ordnen sie als Attribut der Klasse Player zu. Es gibt zwei Arten von Gegnern, nämlich Computergegner und menschliche Gegner. Dabei handelt es sich jeweils um Spieler. Wir verfeinern daher unsere PlayerKlasse, indem wir zwei neue Klassen (CompPlayer) und (Human) ableiten. Ein Computergegner verfolgt eine bestimmte Strategie, die wir als Attribut speichern.
Für unsere Diagramme verwenden wir die folgenden Symbole:KlassennameInterfacenameDarstellung einer KlasseDarstellung eines InterfaceVererbungsbeziehungImplementierung eines InterfaceSonstige Beziehung
Um eine strikte Trennung zwischen Programmlogik und Oberfläche zu gewährleisten, lassen wir zunächst die graphische Oberfläche aus unseren Entwurf weg. Vorläufig haben wir so allein aus der Aufgabenbeschreibung folgende Klassen und Beziehungen abgeleitet:GamePlayerCompPlayerHumanenthältist einverwendet
Nun betrachten wir den zweiten Teil der Aufgabenstellung („Die Regeln“). Es gibt im Spiel eine Menge von Karten, die jeweils eine Nummer und einen Wert besitzen. Dabei fällt auf, dass der Wert einer Karte direkt aus deren Nummer ableitbar ist. Wir modellieren die Nummer daher als Attribut und den Wert als Methode einer Klasse Card. Alle Karten werden zu Beginn des Spiels gemischt und anschließend wird eine Teilmenge der Karten auf die Spieler verteilt. Dies modellieren wir als Beziehungen zwischen Spiel und Karte sowie zwischen Spieler und Karte.
Offenbar gibt es im Spiel 4 Kartenreihen, die maximal 5 Karten aufnehmen können. Eine solche Reihe stellen wir durch die Klasse Row dar. Wir erweitern unser bisheriges „Klassendiagramm“ wie folgt:GamePlayerCompPlayerHumanenthältist einverwendetCardRowbesteht aushathathält
Ein Spieler kann den Spielverlauf durch Auswahl einer seiner Karten gestalten. Danach greifen Regeln, auf die der Spieler zunächst keinen Einfluss hat. Eine Ausnahme bildet die Auswahl der Reihe beim Fall „niedrigste Karte“. Ein Spieler muss Karten kassieren können. Hieraus sind keine neuen Klassen ableitbar, wir merken uns jedoch, dass ein Spieler entsprechende Funktionen (Methoden) anbieten muss.

3.1.1 Attribute und Methoden
In unseren Überlegungen haben wir bereits einige Attribute und notwendige Methoden zu unseren Klassen vorgemerkt. Jetzt wird es Zeit, sich genauer Gedanken über die benötigten Schnittstellen unserer Klassen zu machen.
Die einfachste Klasse stellt eine Karte dar. Es muss eine Karte mit einer gewünschten Nummer erzeugt werden können. Diese Nummer sowie der Wert (Punktzahl) der Karte bleiben im Spiel unverändert und müssen abgefragt werden können (getNumber und getValue). Wir erhalten:CardCard(number: int) {1 number  104}int getNumber()int getValue()Obwohl wir in dieser Darstellung einige get-Methoden angeben, werden get- und set- Methodenin der Klassenbeschreibung meist weggelassen.
Als nächstes schauen wir uns eine Reihe an. Diese besteht aus 0 bis 5 Karten. Es muss eine Karte angelegt werden können (append). Weiterhin sollte man die Anzahl der Karten (numOfCards), den aufsummierten Wert aller Karten (value) sowie die Nummer der obersten Karte (maxNumber) abfragen können. Um das „Kassieren“ von Karten implementieren zu können bieten wir eine Methode an, um die oberste Karte zu entfernen (remove). Auf eine beliebige Karte lässt sich durch die Methode getCardzugreifen. Somit erhalten wir die folgende Klasse:Rowappend(c: Card)int numOfCards()int value()Card remove()int maxNumber()-vector<Card> cardsCard getCard(pos: int)private Member kennzeichnenwir mit einem vorangestellten -
Betrachten wir nun die Klasse Player. Laut Aufgabenstellung soll jeder Spieler einen Namen (name) haben. Ihm müssen Karten zugeteilt werden können (10 Stück), die er auf der Hand hat (handCards). Die aktuelle Punktzahl (Punktzahl vor dem aktuellen Spiel) speichern wir in einem Attribut points. Kassierte Karten bekommt er mittels takeaufgedrückt. Wir speichern solche Karten im Attribut badCards. Nach Abschluss des Spiels werden die kassierten Karten mittels back wieder eingesammelt. Sonstige Aktionen eines Spielers sind die Auswahl einer seiner eigenen Karten (selectCard) und die Auswahl einer aufzunehmenden Reihe (chooseRow). Diese werden völlig unterschiedlich von den zwei Benutzertypen (Mensch, Computer) realisiert, so dass wir diese als abstrakte Methoden kennzeichnen. Um Spieler über die Züge der anderen Spieler zu informieren, fügen wir entsprechende Funktionen hinzu. Wir erhalten:Player-vector<Card> handCards-vector<Card> badCards-int pointsadd(card: Card)take(card: Card)Card back()Card selectCard(rows: Row[]) {abstract}int chooseRow(rows: Row[]) {abstract}-String namevoid infoCard(p: Player, card: Card) {abstract}void infoRow(p: Player, row: Row) {abstract}
Der menschliche Spieler hat keine weiteren Funktionen. Der Computer hat noch ein Attribut für die gerade verwendete Strategie. Wir verzichten an dieser Stelle auf eine Darstellung der Klassen.
Die koordinierende Klasse Game enthält einen Kartenstapel (cards) sowie die 4 Reihen (rows), verwaltet die Spieler (player) und koordiniert das Spiel. Um ein neues Spiel zu beginnen, gibt es eine Methode init. Anschließend können die verschiedenen Spieler hinzugefügt werden (addPlayer). Die Methode start dient dazu, ein neue Runde zu starten. Eine Funktion next wird verwendet, um den nächsten Schritt des Spiels auszuführen. Daraus ergibt sich:Game-vector<Card> cards-vector<Player> playerinit()addPlayer(player: Player)start()-array[Row] rowsnext()

3.1.2 Die graphische Oberfläche
Der bisherige Entwurf reicht bereits aus, um das „6 nimmt!“-Spiel vollständig zu beschreiben. Nun werden noch Klassen für die graphische Oberfläche benötigt. Weiterhin benötigen wir auch noch entsprechende Verbindungen zu den Klassen des eigentlichen Spiels.
Die verschiedenen graphischen Komponenten ordnen wir innerhalb eines Fensters (Klasse MainFrame) an. Zuerst muss es möglich sein, neue Spieler anzulegen. Dies erledigen wir innerhalb eines dafür vorgesehenen Dialogs (PlayerDialog). Diese Klasse benötigt natürlich Zugriff auf die Klasse Player. Die graphische Darstellung der Karten überlassen wir einer eigenen Klasse CardPainter. Diese enthält eine Funktion paintCard, die eine gegebene Karte in ein dafür vorgesehenes Rechteck zeichnet. Dafür eine eigene Klasse zu implementieren, erscheint vielleicht etwas übertrieben, erleichtert aber das spätere Ändern der Kartendarstellung. Das Zeichnen des gesamten Spiels überlassen wir einer Klasse Display. Da diese die aktuelle Spielsituation darstellen soll, benötigt sie Zugriff auf die Klasse Game. Damit alle Informationen auch dargestellt werden können, fügen wir der Klasse Game noch Methoden hinzu, die den aktuellen Zustand des Spiels beschreiben (aktueller Spieler, Anzahl der Minuspunkte für jeden Spieler, Angabe der Reiheninhalte). Um die Umsetzung menschlicher Spieler zu ermöglichen, spendieren wir dieser Klasse auch die Funktionen selectCard und chooseRow. Die Klasse Humanmuss auf diese Funktionen zugreifen können.
Nun müssen wir uns noch überlegen, wie wir die Oberfläche über Änderungen der Spielsituation informieren können, ohne dass unsere Basisklassen Zugriff auf die Oberfläche selbst haben. Dies wird wie folgt modelliert. Ein Interface GameListener enthält eine
Menge von Funktionen, die über Änderungen der aktuellen Spielsituation informieren. Solche Änderungen können sein:
•eine neue Runde wird eröffnet (startRound)
•ein anderer Spieler kommt an die Reihe (currentPlayer(p: Player))
•ein Spieler hat eine Karte (verdeckt) ausgewählt (selectCard(p: Player))
•Karten der Spieler werden aufgedeckt (selectCard(p: Player, c: Card))
•ein Spieler spielt eine Karte aus (play(p: Player, c: Card))
•ein Spieler wählt eine Reihe (playerSelectRow(p: Player, r: Row))
•ein Spieler nimmt eine Reihe auf, da diese voll ist (fullRow(r: Row, p: Player))
•eine Karte wurde an eine Reihe angelegt (dock(r: Row))
•eine Runde wurde beendet (roundEnds())
•ein ganzes Spiel wurde beendet (gameEnds())
Der Klasse Game verpassen wir ein Attribut vom Typ GameListener. Ist dieses gesetzt (nicht null), so wird bei einer Änderung der Spielsituation die passende Funktion aufgerufen. Mittels der Funktion setGameListener kann dieses Attribut gesetzt werden. Unsere Anzeigeklasse (Display) implementiert nun genau dieses Interface. Nach Aufbau der Oberfläche durch die Klasse MainFrame erzeugt diese auch eine Instanz der Klasse Game und ruft setGameListener auf. Somit wird die graphische Oberfläche informiert, obwohl die Basisklassen kein Wissen über die Existenz einer graphischen Oberfläche haben.
Zusammen mit der Oberfläche erhalten wir so den folgenden Entwurf:GamePlayerCompPlayerHumanenthältist einCardRowbesteht aushathathältCardPainterPlayerDialogeditiertstellt darverwendetGameListenerinformiertMainFrameDisplayenthältverwendetruft aufkonfiguriertzeichnet
Einen Lösungsvorschlag, der aus diesen Überlegungen resultiert, zeigt das nächste Kapitel.

4  Entwurf zum „6 nimmt!“-Spiel

4.1  Klassendiagramm
Für die Realisierung des „6 nimmt!“-Spiels ist folgende Programmarchitektur vorgesehen:GamePlayerCompPlayerHumanenthältist einCardRowbesteht aushathathältCardPainterPlayerDialogeditiertstellt darverwendetGameListenerinformiertMainFrameDisplayenthältverwendetruft aufkonfiguriertzeichnet

4.2  Funktionalität der Klassen

4.2.1 CardCardCard(number: int)int getNumber()int getValue()
Die Klasse Card dient zur Beschreibung einer Karte des „6 nimmt!“-Spiels. Sie besitzt eine Nummer sowie einen Wert. Der Wert einer Karte ist direkt aus ihrer Nummer ableitbar. Die
Nummer einer Karte wird im Verlauf des Spiels nicht mehr geändert. Nummer und Wert einer Karte lassen sich abfragen.

4.2.2 RowRow-vector<Card> cardsappend(c: Card)Card remove()int numOfCards()int value()int maxNumber()Card getCard(pos: int)
Eine Reihe wird durch die Klasse Row repräsentiert. Diese besteht aus 0-5 Karten. Sie bietet Funktionen zum Anlegen einer Karte (append) sowie zum Löschen der obersten Karte (remove). Es lassen sich die Anzahl der Karten in der Reihe (numOfCards), der Wert der obersten Karte (maxValue), die Karte an einer gegebenen Position (getCard) sowie der aufsummierte Wert aller enthaltenen Karten (value) abfragen. Zur Darstellung der Folge von Karten wird ein privates Attribut cards verwendet.

4.2.3 PlayerPlayer-vector<Card> handCards-vector<Card> badCards-int points-String nameadd(c: Card)take(c: Card)Card back()Card selectCard(rows: Row[]) {abstract}int chooseRow(rows: Row[]) {abstract}infoCard(p: Player, c: Card) {abstract}infoRow(p: Player, row: Row) {abstract}
Mittels Instanzen der Klasse Player werden die Spieler des Spiels dargestellt. Jeder Spieler hat einen Namen (name) sowie eine Anzahl von Minuspunkten (points). Karten, die momentan auf der Hand sind, werden in einem Attribut (handCards) dargestellt. Die während einer Runde einkassierten Karten werden ebenfalls in einem Attribut (badCards) festgehalten. Um das Austeilen von Karten zu ermöglichen, gibt es die Funktion add, die den Handkarten eine neue Karte hinzufügt. Eine „kassierte“ Karte erhält ein Spieler mittels take. Solche Karten werden am Ende der Runde durch die Funktion back wieder dem Spiel übergeben, wobei eine entsprechende Anpassung der Minuspunkte erfolgt. Ein Spieler kann durch Auswahl einer seiner Karten (selectCard) oder durch Auswahl einer Reihe (chooseRow) ins Spielgeschehen eingreifen. Weiterhin gibt es Funktionen, die einen Spieler über Aktionen von anderen Spielern informieren. Die Implementierungen der letztgenannten Funktionen hängen stark davon ab, ob es sich um einen menschlichen oder einen Computerspieler handelt und werden daher als abstrakt gekennzeichnet.

4.2.4 CompPlayer und Human
Die beiden Klassen CompPlayer und Human sind Unterklassen der Klasse Player und implementieren die dort definierten abstrakten Funktionen auf unterschiedliche Weise.
Die Klasse CompPlayer enthält noch ein zusätzliches Attribut, um die gerade verfolgte Strategie zu speichern.

4.2.5 GameGame-vector<Card> cards-vector<Player> player-Row[] rowsinit()addPlayer(p: Player)start()next()int numOfPlayers()Player getPlayer(pos: int)Row getRow(pos: int) Player currentPlayer()setGameListener(g: GameListener)
Die Klasse Game organisiert das gesamte Spiel. Der Kartenstapel, von dem die Karten verteilt werden, ist in einem Attribut cards festgehalten. Die am Spiel teilnehmenden Spieler werden im Attribut player gespeichert. Mittels addPlayerkann ein neuer Spieler hinzugefügt werden. Die Funktion init dient dazu, die Ausgangskonfiguration eines Spiels (ohne Spieler) herzustellen. Mittels start wird eine neue Runde gestartet. Diese Methode sollte aufgerufen werden, wenn alle Karten ausgespielt wurden, jedoch noch nicht die erforderliche Maximalpunktzahl erreicht wurde, um das gesamte Spiel zu beenden. Durch next wird ein vollständiger Schritt des Spiels angestoßen. Dabei spielen alle Spieler ihre Karten aus, die dann automatisch an die Kartenstapel angelegt werden, falls dies möglich ist. Falls nicht, wird die übervolle oder ausgewählte Reihe dem „Opfer“ zugedacht. Weitere Funktionen dienen dazu, sich über den aktuellen Zustand des Spiels zu informieren. Falls man über Änderungen der Spielsituation informiert werden möchte, registriert man einen GameListener bei der Klasse Game.

4.2.6 GameListenerGameListenerstartRound()currentPlayer(p: Player)selectCard(p: Player)selectCard(p: Player, c: Card)play(p: Player, c: Card)playerSelectsRow(p: Player, r : Row)fullRow(r: Row, p: Player)dock(r: Row)roundEnds()gameEnds()
Dieses Interface wird verwendet, um Interessenten über Änderungen im aktuellen Spiel zu informieren. Es enthält eine Reihe von Funktionen, die aufgerufen werden, wenn die passende Veränderung der Spielsituation eingetreten ist. Den Beginn und das Ende einer Runde signalisiert der Aufruf der Funktion startRound bzw. roundEnds. Wechselt der Spieler, der momentan an der Reihe ist, wird currentPlayer aufgerufen. Die verdeckte Auswahl einer Karte wird mittels selectCard angezeigt. Wird dieser Funktion zusätzlich noch die selektierte Karte übergeben, so entspricht dies dem Aufdecken dieser Karte durch den Spieler. Die Funktion play wird verwendet, um das Ausspielen einer Karte durch einen Spieler kenntlich zu machen. Muss ein Spieler eine volle Reihe aufnehmen, wird fullRow aufgerufen. Das Aufnehmen einer selbst
gewählten Reihe ist durch playerSelectsRow mitzubekommen. Die Funktion dock entspricht dem Anlegen einer Karte an eine Reihe. Das Ende des Spiels erfährt ein GameListener durch den Aufruf seiner Funktion gameEnds.

4.2.7 CardPainter
Diese Klasse enthält (vorerst) nur eine einzige Funktion paintCard. Neben der Karte erhält diese Funktion noch einen Graphikkontext, auf dem die Karte gemalt werden soll, sowie die Position und Größe eines Rechtecks, in das die Karte zu zeichnen ist.

4.2.8 Playerdialog
Diese Klasse stellt ein Fenster dar, in dem nach Auswahl der Anzahl der Mitspieler jedem Spieler ein Name zugewiesen werden kann. Außerdem kann hier angegeben werden, ob es sich um einen menschlichen oder um einen Computergegner handelt. Nach Bestätigung der Einstellungen über einen entsprechenden Button werden die Spieler gemäß diesen Einstellungen erzeugt. Nach außen hin bietet diese Klasse nur eine Funktion createPlayers, die einen Vektor von Spielern zurückgibt.

4.2.9 DisplayDisplay-Game gameCard selectCard()Row chooseRow()
Diese Klasse (abgeleitet von JPanel) übernimmt die gesamte Darstellung des Spiels. Um über Änderungen der Spielsituationen informiert zu werden, implementiert sie das GameListener Interface. Die manuelle Auswahl von Karten oder das Aufnehmen einer Reihe wird hier ebenfalls implementiert.

4.2.10 MainFrame
Hier wird das Hauptfenster des Spiels angezeigt. Es organisiert die verschiedenen Elemente des Spiels. Die restlichen Klassen des „6 nimmt!“-Spiels verwenden keine Funktionen dieser Klasse.

4.3  Beziehungen zwischen den Klassen
Die Vererbungsbeziehungen im Diagramm wurden bereits in den Klassenbeschreibungen erläutert. Da die Klasse Game für den Ablauf des Spiels verantwortlich ist, ist dies auch die Klasse, die einen evtl. registrierten GameListener informieren kann. Dabei werden
alle durch den GameListener angebotenen Funktionen verwendet. Da ein Spiel einen Kartenstapel hat und dafür verantwortlich ist, Karten zwischen diesem Stapel, Spielern und Reihen auszutauschen, ist eine Beziehung zu den Klassen Card, Player und Rownotwendig. Eine Reihe ist eine geordnete Menge von 0-5 Karten. Um ihren Gesamtwert und ihre maximale Nummer berechnen zu können, muss sich eine Reihe auf Funktionen der Klasse Card abstützen.
Um eine Karte korrekt darstellen zu können, muss die CardPainter-Klasse Wert und Nummer einer Karte erfahren können. Die Klasse Display nutzt die Funktionalität von CardPainter, um die sichtbaren Karten des Spiels zu zeichnen. Bei wem sich die sichtbaren Karten des Spiels aufhalten, „weiß“ nur die Klasse Game. Die Display-Klasse ist daher auf diese Klasse angewiesen, um das Spiel darzustellen.
Der PlayerDialog erzeugt und manipuliert die Spieler und benötigt daher Zugriff auf diese Klasse. Der Dialog selbst wird von der Klasse MainFrame aufgerufen. Die vom PlayerDialog erzeugten Spieler werden durch eine Instanz der Klasse MainFrame an das Spiel weitergereicht (addPlayer). Weiterhin wird auch die Spielsteuerung hier bewerkstelligt (Aufruf von init, start und next). Innerhalb des Hauptfensters ist auch eine Instanz der Klasse Display angeordnet, die die graphische Ausgabe übernimmt. Da ein menschlicher Spieler nur über die Oberfläche seine Karten und Reihen auswählen kann, ist die Klasse Human auf die Funktionen selectCard und chooseRow der Klasse Displayangewiesen.

4.3.1 Pakete
Wir teilen unser Projekt in zwei Pakete auf. Ein Paket game übernimmt die Spielelogik und soll keine Klassen des anderen Pakets verwenden. Das zweite Paket gui ist für die graphische Darstellung sowie die Kommunikation mit menschlichen Spielern verantwortlich. Zum Paket game zählen die folgenden Klassen: Card, Row, CompPlayer, Player, Game, GameListener. Alle anderen Klassen werden im Paket gui untergebracht.
